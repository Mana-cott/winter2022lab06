import java.util.Random;
public class Die{
	//fields
	private int pips;
	private Random rand;
	//constructor
	public Die(){
		this.pips = 1;
		this.rand = new Random();
	}
	//get method
	public int getPips(){
		return this.pips;
	}
	//rolling dice
	public void roll(){
		this.pips = rand.nextInt(6)+1;
	}
	//toString method
	public String toString(){
		return "Your pip is: " + this.pips;
	}
}