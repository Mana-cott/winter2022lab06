public class Board{
	//fields
	private Die dice1;
	private Die dice2;
	private boolean[] closedTiles;
	//constructor
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.closedTiles = new boolean[12];
	}
	//toString method
	public String toString(){
		String updateMessage = " ";
		for(int i = 0; i < closedTiles.length; i++){
			if(closedTiles[i] == true){
				updateMessage = updateMessage + " X";
			}
			else{
				updateMessage = updateMessage + " " + (i+1);
			}
		}
		return updateMessage;
	}
	//game functionality
	public boolean playATurn(){
		this.dice1.roll();
		this.dice2.roll();
		System.out.println(dice1.toString());
		System.out.println(dice2.toString());
		int sum = dice1.getPips() + dice2.getPips();
		if(this.closedTiles[(sum - 1)] == false){
			this.closedTiles[(sum - 1)] = true;
			System.out.println("Closing tile: ~ " + (sum));
			return false;
		}
		else{
			System.out.println("Tile has already been shut: " + sum);
			return true;
		}
	}
}