public class ShutTheBox{
	public static void main(String[] args){
		System.out.println("Welcome to the shut in the box game!");
		Board gameBoard = new Board();
		boolean gameOver = false;
		do{
			System.out.println("\n");
			System.out.println("Player 1's turn");
			System.out.println(gameBoard.toString());
			if(gameBoard.playATurn() == true){
				//win message
				System.out.println("Player 2 wins!!");
				System.out.println("    _______         ");
				System.out.println("   /\\       \\     ");
				System.out.println("  /()\\  ()   \\    ");
				System.out.println(" /    \\_______\\   "); 
				System.out.println(" \\    /()     /    ");
				System.out.println("  \\()/   ()  /     ");
				System.out.println("   \\/_____()/      "); 
				System.out.println("                    ");
				gameOver = true;
			}
			else{
				System.out.println("\n");
				System.out.println("Player 2's turn");
				System.out.println(gameBoard.toString());
				if(gameBoard.playATurn() == true){
					//win message
					System.out.println("Player 1 wins!!");
					System.out.println("    _______         ");
					System.out.println("   /\\       \\     ");
					System.out.println("  /()\\  ()   \\    ");
					System.out.println(" /    \\_______\\   "); 
					System.out.println(" \\    /()     /    ");
					System.out.println("  \\()/   ()  /     ");
					System.out.println("   \\/_____()/      "); 
					System.out.println("                    ");
					gameOver = true;
				}
			}
		}while(gameOver != true);
	}
}